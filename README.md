# Discord One Dark
Discord One Dark is a [Discord](https://discordapp.com/) theme inspired by [Atoms One Dark theme](https://github.com/atom/atom/tree/master/packages/one-dark-syntax).

Many features are still missing, but you can always create merge requests with additions to the theme.

![this would be a screenshot, but you can't see it](screenshot.png)

## Installation
- Install [PowerCord](https://powercord.dev/)
- Change your directory: `cd <powercord install directory>/src/Powercord/themes`
- Download the theme: `git clone https://gitlab.com/frozo/discord-onedark`
- Restart discord (`CTRL-R`)
- (Optional: Install the monospaced font used by default [Iosevka SS09](https://github.com/be5invis/Iosevka/releases); to can change the font to something else, see below)

## Configuration
You can configure this theme to your liking by editing the `_custom.scss` file.

## Help
You can always ask for help in the [discord guild](https://discord.gg/EdwmUsJ).
For feature requests or bug reports, please open an issue in this repository.
